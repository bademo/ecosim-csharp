﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EcoSim.Banks;

namespace EcoSim.Humans
{
	abstract class Human
	{
		readonly public string id;
		private string bankid;
		int cash;
		public Human()
		{
			id = new Guid().ToString().Replace("-", "");
		}
		public bool pay(int amount,Human human)
		{
			if (cash >= amount)
			{
				cash -= amount;
				human.cash += amount;
				return true;
			}
			else if (Bank.banks[bankid].GetPersonBal(id, out int Mamount))
			{
				if (Mamount >= amount)
				{
					cash -= amount;
					human.cash += amount;
					return true;
				}
				else
				{
					return false;
				}
			}
			return false;
		}
		public bool Take(int amount)
		{
			if (cash >= amount)
			{
				this.cash -= amount;
				return true;
			}
			else
			{
				return false;
			}
		}
	}
}
