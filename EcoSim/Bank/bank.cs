﻿using System;
using System.Collections.Generic;
using EcoSim.Humans;

namespace EcoSim.Banks
{
	class Bank
	{
		readonly public string id;
		public static Dictionary<string,Bank> banks;
		Dictionary<string,int> bals;
		readonly public string name;

		public Bank()
		{
			id = new Guid().ToString().Replace("-", "");
			banks.Add(id, this);
		}
		public void AddPerson(Human human, int initBal)
		{
			if (human.Take(initBal)) bals.Add(human.id, initBal);
		}
		public bool GetPersonBal(string id,out int bal)
		{
			if (bals.ContainsKey(id))
			{
				bal = bals[id];
				return true;
			}
			else
			{
				bal = 0;
				return false;
			}
		}
	}
}
